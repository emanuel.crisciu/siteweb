<!DOCTYPE html>
<html>

<head>
    <title>Poli Timișoara</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styleMatches.css">
    <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
</head>

<body>
    <div class="banner1">
        <div class="navbar">
            <div class="navbar-left">
                <div class="navbar-left-text">POLI</div>
                <img class="navbar-logo" src="photos/poliLogo.png">
                <div class="navbar-left-text">TIMIȘOARA</div>
            </div>
            <div class="navbar-middle">
                <a href="index.php">
                    <div class="navbar-middle-button">ACASĂ</div>
                </a>
                <a href="echipa.php">
                    <div class="navbar-middle-button">ECHIPA</div>
                </a>
                <a href="meciuri.php">
                    <div class="navbar-middle-button">MECIURI</div>
                </a>
                <a href="shop.php">
                    <div class="navbar-middle-button">SHOP</div>
                </a>
            </div>
            <div class="navbar-right">
                <a href="logout.php">
                    <div class="navbar-login-button">LOGOUT</div>
                </a>
            </div>
        </div>

        <div class="matches-zone-title">CALENDARUL POLITEHNICII TIMIȘOARA</div>
        <div class="matches-zone">

            <?php @include 'config.php'; 
                    $select = mysqli_query($conn, "SELECT * FROM matches");
                    while($row = mysqli_fetch_assoc($select)){ ?>
            <div class="match">
                <div class="match-competition-logo">
                    <img class="match-competition-logo-image" src="match_uploaded/<?php echo $row['logo']; ?>">
                </div>
                <div class="match-venue">
                    <?php echo $row['stadium']; ?>
                    <div class="match-time"><?php echo $row['time']; ?></div>
                </div>
                <div class="match-team">
                    <div class="match-team-logo">
                        <img class="match-team-logo-image" src="match_uploaded/<?php echo $row['team1logo']; ?>">
                    </div>
                    <div class="match-team-name"><?php echo $row['team1']; ?></div>
                </div>
                <div class="match-versus">VS</div>
                <div class="match-team">
                    <div class="match-team-logo">
                        <img class="match-team-logo-image" src="match_uploaded/<?php echo $row['team2logo']; ?>">
                    </div>
                    <div class="match-team-name"><?php echo $row['team2']; ?></div>
                </div>
                <div class="match-score"><?php echo $row['score']; ?></div>
            </div>

            <?php } ?>

        </div>

        <div class="news-header">Știri</div>   
        <div class="news-zone">
            <div class="news">
                <a href="stadionNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/stadion.webp"></div></a>
                <div class="news-title">Noul stadion din Timișoara</div>
            </div>
            <div class="news">
                <a href="celtic43News.html"><div class="news-photo"><img class="news-photo-image" src="photos/Poli-Celtic-1980.jpg"></div></a>
                <div class="news-title">43 de ani de la eliminarea lui Celtic</div>
            </div>
            <div class="news">
                <a href="cupaMondialaNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/news1-photo.jpg"></div></a>
                <div class="news-title">Cupa Mondială și-a stabilit învingătoarea</div>
            </div>
        </div>
        <div class="map-text">Unde ne găsiți?</div>
        <div class="map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2784.532772010323!2d21.241458875876432!3d45.740464114964084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47455d92ebd3d733%3A0xf3fdfa7034ef15b8!2sStadionul%20Dan%20P%C4%83ltini%C8%99anu!5e0!3m2!1sro!2sro!4v1696682882466!5m2!1sro!2sro"
                width="1300" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
        <div class="partners-zone">
            <div class="partners-title">PARTENERI</div>
            <div class="partners">
                <div class="partners-photo"><a href="https://www.errea.com/world/" target="_blank"><img class="partners-photo-image" src="photos/ERREA-logo.png"></a></div>
                <div class="partners-photo"><a href="https://www.upt.ro/" target="_blank"><img class="partners-photo-image" src="photos/logo_UPT.jpg"></div></a>
                <div class="partners-photo"><a href="https://www.druckeria.ro/" target="_blank"><img class="partners-photo-image" src="photos/Druckeria-logo.png"></div></a>
                <div class="partners-photo"><a href="https://www.mewi.ro/" target="_blank"><img class="partners-photo-image" src="photos/MEWI.jpg"></div></a>
                <div class="partners-photo"><a href="https://www.casa-bunicii.ro/" target="_blank"><img class="partners-photo-image" src="photos/restaurant.png"></div></a>
                <div class="partners-photo"><a href="https://apuseana.ro/" target="_blank"><img class="partners-photo-image" src="photos/apuseana.png"></div></a>
                <div class="partners-photo"><a href="https://www.unibet.ro/" target="_blank"><img class="partners-photo-image" src="photos/unibet2429.jpg"></div></a>
            </div>
        </div>
    </div>
</body>

</html>