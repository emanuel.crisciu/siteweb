<?php

@include 'config.php';

if(isset($_POST['order_btn'])){

   $name = $_POST['name'];
   $phone = $_POST['phone'];
   $pay_method = $_POST['pay_method'];
   $address = $_POST['address'];
   $city = $_POST['city'];
   $country = $_POST['country'];
   $postal_code = $_POST['postal_code'];

   $cart_query = mysqli_query($conn, "SELECT * FROM `cart`");
   $price_total = 0;
   if(mysqli_num_rows($cart_query) > 0){
      while($product_item = mysqli_fetch_assoc($cart_query)){
         $product_name[] = $product_item['name'] .' ('. $product_item['quantity'] .') ';
         $product_price = number_format($product_item['price'] * $product_item['quantity']);
         $price_total += $product_price;
      };
   };

   $total_product = implode(', ',$product_name);

   $detail_query = mysqli_query($conn, "INSERT INTO `orders`(name, phone, pay_method, address, city, country, postal_code, total_price, total_products) 
   VALUES('$name','$phone','$pay_method','$address','$city','$country','$postal_code','$price_total', '$total_product')") or die('query failed');

   if($cart_query && $detail_query){
      echo "
      <div class='order-message-container'>
      <div class='message-container'>
         <h3>Comanda a fost plasată! Rezumatul comenzii:</h3>
         <div class='order-detail'>
            <span style='font-size: 18px;'>".$total_product."</span>
            <span class='total' style='background-color: rgb(92, 32, 92); font-weight: 500;'> Total de plată: ".$price_total." lei  </span>
         </div>
         <div class='customer-details'>
            <p> Numele și prenumele : <span style='color: rgb(92, 32, 92); font-weight: 500;'>".$name."</span> </p>
            <p> Numărul de telefon : <span style='color: rgb(92, 32, 92); font-weight: 500;'>".$phone."</span> </p>
            <p> Adresa : <span style='color: rgb(92, 32, 92); font-weight: 500;'>".$address.", ".$city.", ".$country." - ".$postal_code."</span> </p>
            <p> Metoda de plată : <span style='color: rgb(92, 32, 92); font-weight: 500;'>".$pay_method."</span> </p>
         </div>
         <a href='cart.php?delete_all' class='order-btn'>CONTINUAȚI CUMPĂRĂTURILE</a></a>

         </div>
      </div>
      ";
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleOrder.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />


</head>
<body>
<div class="container">

<section class="checkout-form">

   <h1 class="heading">Finalizați comanda</h1>
   <?php
         $select_cart = mysqli_query($conn, "SELECT * FROM `cart`");
         $total = 0;
         $grand_total = 0;
         if(mysqli_num_rows($select_cart) > 0){
            while($fetch_cart = mysqli_fetch_assoc($select_cart)){
            $total_price = number_format($fetch_cart['price'] * $fetch_cart['quantity']);
            $grand_total = $total += $total_price;
      ?>
      <div style="text-align: center; margin-bottom: 20px; color: rgb(92, 32, 92); font-weight: 500; padding: 4px 0px; background-color: rgba(229, 229, 229, 0.721); border-radius: 15px;">
         <span style="font-size: 20px;"><?= $fetch_cart['name']; ?> (<?= $fetch_cart['quantity']; ?>) </span>
      </div>
      <?php
         }
      }else{
         echo "<div class='display-order'><span>your cart is empty!</span></div>";
      }
      ?>
      <h1 class="heading"> Total de plată: <?= $grand_total; ?> lei </h1>

   <form action="" method="post">

      <div class="flex">
         <div class="inputBox">
            <p>Numele și prenumele</p>
            <input type="text" placeholder="Introduceți numele și prenumele" name="name" required>
         </div>
         <div class="inputBox">
            <p>Telefon</p>
            <input type="text" placeholder="Introduceți nr tel" name="phone" required>
         </div>
         <div class="inputBox">
            <p>Metodă de plată</p>
            <select name="pay_method">
               <option value="Plată la livrare" selected>Plată la livrare</option>
            </select>
         </div>
         <div class="inputBox">
            <p>Adresă</p>
            <input type="text" placeholder="Introduceți adresa" name="address" required>
         </div>
         <div class="inputBox">     
            <p>Oraș  </p>
            <input type="text" placeholder="Introduceți orașul" name="city" required>
         </div>
         <div class="inputBox">
            <p>Țara</p>
            <input type="text" placeholder="Introduceți țara" name="country" required>
         </div>
         <div class="inputBox">
            <p>Cod poștal</p>
            <input type="text" placeholder="codul trebuie să conțină 6 cifre" name="postal_code" required>
         </div>
      </div>
      <input type="submit" value="PLASEAZĂ COMANDA!" name="order_btn" class="order-btn">
      <a href="cart.php" class="order-btn">ÎNAPOI</a>
   </form>

</section>

</div>

   
</body>
</html>