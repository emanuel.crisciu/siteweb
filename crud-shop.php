<?php

@include 'config.php';

if(isset($_POST['add_product'])){

   $product_name = $_POST['product_name'];
   $product_price = $_POST['product_price'];
   $product_image = $_FILES['product_image']['name'];
   $product_image_tmp_name = $_FILES['product_image']['tmp_name'];
   $product_image_folder = 'product_uploaded/'.$product_image;

   if(empty($product_name) || empty($product_price) || empty($product_image))
   {
    $message[] = 'Completați toate câmpurile';
    }
    else{
        $insert = "INSERT INTO products(name, price, image) VALUES('$product_name', '$product_price', '$product_image')";
        $upload = mysqli_query($conn,$insert);
        if($upload){
           move_uploaded_file($product_image_tmp_name, $product_image_folder);
           $message[] = 'Produsul a fost adăugat cu succes!';
        }else{
           $message[] = 'Nu s-a putut adăuga produsul.';
        }
     }

};

if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM products WHERE id = $id");
    header('location:crud-shop.php');
};


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Poli Timișoara</title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
        <link rel="stylesheet" href="styleCrud.css">
        <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
    </head>
    <body>
    <?php
         if(isset($message))
        {
            foreach($message as $message)
            {
                    echo '<span class="message">'.$message.'</span>';
            }
        }

?>
    <div class="container">

        <div class="admin-product-form-container">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <h3>Adaugă un nou produs</h3>
                <label style="font-family: 'Ubuntu', sans-serif;
                font-size: 15px;
                padding-top: 5px;">Nume produs<input type="text" placeholder="Introdu numele produsului" name="product_name" class="box">
                <label>Preț produs<input type="number" min="0" placeholder="Introdu prețul" name="product_price" class="box">
                <label>Imagine produs<input type="file" accept="image/png, image/jpeg, image/jpg" name="product_image" class="box">
                <input type="submit" class="btn" name="add_product" value="ADAUGĂ PRODUS">
                
            </form>
            
        </div>

    <?php

        $select = mysqli_query($conn, "SELECT * FROM products");
   
    ?>
   <div class="product-display">
      <table class="product-display-table">
         <thead>
         <tr>
            <th>ID</th>
            <th>Imagine produs</th>
            <th>Nume produs</th>
            <th>Preț produs</th>
            <th>Opțiuni</th>
         </tr>
         </thead>
         <?php while($row = mysqli_fetch_assoc($select)){ ?>
         <tr>
            <td><?php echo $row['id']; ?></td>
            <td><img src="product_uploaded/<?php echo $row['image']; ?>" height="100" alt=""></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['price']; ?></td>
            <td>
               <a href="crud-shop-update.php?edit=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-edit"></i> edit </a>
               <a href="crud-shop.php?delete=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-trash-o"></i> delete </a>
            </td>
         </tr>
         <?php } ?>
      </table>
   </div>

   <a href="crud-edit.html" class="btn">ÎNAPOI</a>

    </div>
    </body>
</html>