<?php

@include 'config.php';

if(isset($_POST['add_player'])){

   $player_name = $_POST['player_name'];
   $player_nr = $_POST['player_nr'];
   $player_position = $_POST['player_position'];
   $player_image = $_FILES['player_image']['name'];
   $player_image_tmp_name = $_FILES['player_image']['tmp_name'];
   $player_image_folder = 'player_uploaded/'.$player_image;

   if(empty($player_name) || empty($player_nr) || empty($player_position) || empty($player_image))
   {
    $message[] = 'Completați toate câmpurile';
    }
    else{
        $insert = "INSERT INTO players(name, number, position, image) VALUES('$player_name', '$player_nr', '$player_position','$player_image')";
        $upload = mysqli_query($conn,$insert);
        if($upload){
           move_uploaded_file($player_image_tmp_name, $player_image_folder);
           $message[] = 'Jucătorul a fost adăugat cu succes!';
        }else{
           $message[] = 'Nu s-a putut adăuga jucătorul.';
        }
     }

};

if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM players WHERE id = $id");
    header('location:crud-player.php');
};


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Poli Timișoara</title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
        <link rel="stylesheet" href="styleCrud.css">
        <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
    </head>
    <body>
    <?php
         if(isset($message))
        {
            foreach($message as $message)
            {
                    echo '<span class="message">'.$message.'</span>';
            }
        }

?>
    <datalist id="positions">
            <option value="PORTAR">
            <option value="FUNDAȘ">
            <option value="MIJLOCAȘ">
            <option value="ATACANT">
    </datalist>

    <div class="container">

        <div class="admin-product-form-container">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <h3>Adaugă un nou jucător</h3>
                <label style="font-family: 'Ubuntu', sans-serif;
                font-size: 15px;
                padding-top: 5px;">Nume jucător<input type="text" placeholder="Introdu numele jucătorului" name="player_name" class="box">
                <label>Număr echipament<input type="number" min="0" max="99" placeholder="Introdu numărul" name="player_nr" class="box">
                <label>Poziție jucător<input list="positions" placeholder="Alege poziția jucătorului" name="player_position" class="box">
                <label>Foto echipament<input type="file" accept="image/png, image/jpeg, image/jpg" name="player_image" class="box">
                <input type="submit" class="btn" name="add_player" value="ADAUGĂ JUCĂTOR">
                
            </form>

        </div>

    <?php

        $select = mysqli_query($conn, "SELECT * FROM players");
   
    ?>
   <div class="product-display">
      <table class="product-display-table">
         <thead>
         <tr>
            <th>ID</th>
            <th>Foto echipament</th>
            <th>Nume jucător</th>
            <th>Număr echipament</th>
            <th>Poziție</th>
            <th>Opțiuni</th>
         </tr>
         </thead>
         <?php while($row = mysqli_fetch_assoc($select)){ ?>
         <tr>
            <td><?php echo $row['id']; ?></td>
            <td><img src="player_uploaded/<?php echo $row['image']; ?>" height="100" alt=""></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['number']; ?></td>
            <td><?php echo $row['position']; ?></td>
            <td>
               <a href="crud-player-update.php?edit=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-edit"></i> edit </a>
               <a href="crud-player.php?delete=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-trash-o"></i> delete </a>
            </td>
         </tr>
         <?php } ?>
      </table>
   </div>

   <a href="crud-edit.html" class="btn">ÎNAPOI</a>

    </div>
    </body>
</html>