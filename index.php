<?php

session_start();

if (isset($_SESSION["user_id"])) {
    
    $mysqli = require __DIR__ . "/database.php";
    
    $sql = "SELECT * FROM user
            WHERE id = {$_SESSION["user_id"]}";
            
    $result = $mysqli->query($sql);
    
    $user = $result->fetch_assoc();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Poli Timișoara</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styleIndex.css">
    <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
</head>
<body>
    
    <?php if (isset($user)): ?>
        
  <!DOCTYPE html>
  <html>

    <head>
        <title>Poli Timișoara</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styleIndex.css">
        <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="banner1">
            <div class="navbar">
                <div class="navbar-left">
                    <div class="navbar-left-text">POLI</div>
                    <img class="navbar-logo" src="photos/poliLogo.png">
                    <div class="navbar-left-text">TIMIȘOARA</div>
                </div>
                <div class="navbar-middle">
                    <a href="index.php">
                        <div class="navbar-middle-button">ACASĂ</div>
                    </a>
                    <a href="echipa.php">
                        <div class="navbar-middle-button">ECHIPA</div>
                    </a>
                    <a href="meciuri.php">
                        <div class="navbar-middle-button">MECIURI</div>
                    </a>
                    <a href="shop.php">
                        <div class="navbar-middle-button">SHOP</div>
                    </a>
                </div>
                <div class="navbar-right">
                    <a href="logout.php">
                        <div class="navbar-login-button">LOGOUT</div>
                    </a>
                </div>
            </div>
            <div class="welcome-message">Bine ați venit în aplicația web a clubului de fotbal Poli Timișoara!</div>

            <div class="history-zone">
                <div class="history-title">ISTORIA CLUBULUI</div>
                <div class="history">
                    <div class="history-photo"><img class="history-photo-image" src="photos/istorie"></div>
                    <div class="history-text-box">
                        <ul class="history-text">
                            <li>Fondarea: Clubul a fost fondat în 1921, fiind unul dintre cele mai vechi cluburi de fotbal din România. 
                                Acesta a fost înființat ca o secție a Universității Politehnica din Timișoara.</li>
                            <li>Succese timpurii: Echipa a avut succes în anii '40 și '50, când a câștigat mai multe titluri de campioană a 
                                României și a ajuns în sferturile de finală ale Cupei Campionilor Europeni (predecesorul Ligii Campionilor UEFA).</li>
                            <li>Revigorarea și declinul: După Revoluția din 1989, clubul a revenit la denumirea sa tradițională de Politehnica 
                                Timișoara și a avut câteva sezoane bune în prima divizie a fotbalului românesc. Cu toate acestea, din cauza problemelor
                                financiare și administrative, clubul a întâmpinat dificultăți în a rămâne competitiv.</li>
                            <li>Performanța: Poli a reușit să elimine din cupele europene echipe precum Celtic Glasgow (1980), Atletico Madrid (1990) 
                                și Shakhtar Donetsk (2009). De asemenea, echipa s-a bătut la câștigarea campionatului în perioada 2008-2011, terminând 
                                pe poziția secundă de două ori în aceste sezoane.
                            </li>
                            <li>Retrogradarea și problemele financiare: În 2012, Poli Timișoara a fost retrogradată în liga a doua din cauza 
                                problemelor financiare și a problemelor legate de licența de participare. Clubul a trecut printr-o serie de schimbări 
                                de proprietate și a jucat în diviziile inferioare ale fotbalului românesc. În prezent, clubul evoluează în liga a 
                                treia, jucând cu speranța că într-o zi va reveni la perioada sa de glorie.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="news-header">Știri</div>
            <div class="news-zone">
                <div class="news">
                    <a href="stadionNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/stadion.webp"></div></a>
                    <div class="news-title">Noul stadion din Timișoara</div>
                </div>
                <div class="news">
                    <a href="victorieNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/fanionPoli.jpeg"></div></a>
                    <div class="news-title">Poli înregistrează o nouă victorie</div>
                </div>
                <div class="news">
                    <a href="liga3News.html"><div class="news-photo"><img class="news-photo-image" src="photos/news2-photo.jpg"></div></a>
                    <div class="news-title">Poli debutează în Liga a 3-a</div>
                </div>
                <div class="news">
                    <a href="nouaEchipaNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/adunare.jpeg"></div></a>
                    <div class="news-title">Un nou drum, o nouă echipă</div>
                </div>
                <div class="news">
                    <a href="celtic43News.html"><div class="news-photo"><img class="news-photo-image" src="photos/Poli-Celtic-1980.jpg"></div></a>
                    <div class="news-title">43 de ani de la eliminarea lui Celtic</div>
                </div>
                <div class="news">
                    <a href="cupaMondialaNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/news1-photo.jpg"></div></a>
                    <div class="news-title">Cupa Mondială și-a stabilit învingătoarea</div>
                </div>
            </div>
            <div class="map-text">Unde ne găsiți?</div>
            <div class="map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2784.532772010323!2d21.241458875876432!3d45.740464114964084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47455d92ebd3d733%3A0xf3fdfa7034ef15b8!2sStadionul%20Dan%20P%C4%83ltini%C8%99anu!5e0!3m2!1sro!2sro!4v1696682882466!5m2!1sro!2sro"
                    width="1300" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="partners-zone">
                <div class="partners-title">PARTENERI</div>
                <div class="partners">
                    <div class="partners-photo"><a href="https://www.errea.com/world/" target="_blank"><img class="partners-photo-image" src="photos/ERREA-logo.png"></a></div>
                    <div class="partners-photo"><a href="https://www.upt.ro/" target="_blank"><img class="partners-photo-image" src="photos/logo_UPT.jpg"></div></a>
                    <div class="partners-photo"><a href="https://www.druckeria.ro/" target="_blank"><img class="partners-photo-image" src="photos/Druckeria-logo.png"></div></a>
                    <div class="partners-photo"><a href="https://www.mewi.ro/" target="_blank"><img class="partners-photo-image" src="photos/MEWI.jpg"></div></a>
                    <div class="partners-photo"><a href="https://www.casa-bunicii.ro/" target="_blank"><img class="partners-photo-image" src="photos/restaurant.png"></div></a>
                    <div class="partners-photo"><a href="https://apuseana.ro/" target="_blank"><img class="partners-photo-image" src="photos/apuseana.png"></div></a>
                    <div class="partners-photo"><a href="https://www.unibet.ro/" target="_blank"><img class="partners-photo-image" src="photos/unibet2429.jpg"></div></a>
                </div>
            </div>
        </div>
    </body>

  </html>
        
    <?php else: ?>
        <!DOCTYPE html>
        <html>
            <head>
                <title>Poli Timișoara</title>
                <meta charset="utf-8">
                <link rel="stylesheet" href="styleIndex.css">
                <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
                <link rel="preconnect" href="https://fonts.googleapis.com">
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
                <link rel="preconnect" href="https://fonts.googleapis.com">
                <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
                <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
            </head>
            <body>
                <div style="margin-top: 20px; display: flex; flex-direction: column; align-items: center; justify-content: center;
                font-family: 'Ubuntu', sans-serif; color: rgb(92, 32, 92);">
                    <span style="font-weight: 700; font-size: 30px; margin-bottom: 50px;">
                        BINE AȚI VENIT ÎN APLICAȚIA WEB A CLUBULUI POLI TIMIȘOARA!
                    </span>
                    <img src="photos/poliLogo.png">
                    <div style="margin-top: 50px; display: flex; align-items: center; justify-content: center; font-family: 'Ubuntu', sans-serif; 
                    color: rgb(92, 32, 92); font-weight: 500; font-size: 20px;">
                        <div style="margin-right: 15px; padding: 7px 15px 7px 15px; border: 2px solid rgb(92, 32, 92); border-radius: 20px;">
                            <a href="login.php">LOG IN</a>
                        </div>
                        <div style="margin-left: 15px; padding: 7px 15px 7px 15px; border: 2px solid rgb(92, 32, 92); border-radius: 20px;">
                            <a href="signup.html">SIGN UP</a>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    <?php endif; ?>
    
</body>
</html>