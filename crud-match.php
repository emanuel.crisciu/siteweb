<?php

@include 'config.php';

if(isset($_POST['add_match'])){

   $match_stadium = $_POST['match_stadium'];
   $match_time = $_POST['match_time'];
   $match_team1 = $_POST['match_team1'];
   $match_team2 = $_POST['match_team2'];
   $match_score = $_POST['match_score'];

   $match_logo = $_FILES['match_logo']['name'];
   $match_logo_tmp_name = $_FILES['match_logo']['tmp_name'];
   $match_logo_folder = 'match_uploaded/'.$match_logo;

   $match_logo_team1 = $_FILES['match_logo_team1']['name'];
   $match_logo_team1_tmp_name = $_FILES['match_logo_team1']['tmp_name'];
   $match_logo_team1_folder = 'match_uploaded/'.$match_logo_team1;

   $match_logo_team2 = $_FILES['match_logo_team2']['name'];
   $match_logo_team2_tmp_name = $_FILES['match_logo_team2']['tmp_name'];
   $match_logo_team2_folder = 'match_uploaded/'.$match_logo_team2;

   if(empty($match_stadium) || empty($match_time) || empty($match_team1) || empty($match_team2) || empty($match_score) || empty($match_logo) || empty($match_logo_team1) || empty($match_logo_team2))
   {
    $message[] = 'Completați toate câmpurile';
    }
    else{
        $insert = "INSERT INTO matches(logo, stadium, time, team1, team1logo, team2, team2logo, score) VALUES('$match_logo', '$match_stadium', '$match_time', '$match_team1', '$match_logo_team1', '$match_team2', '$match_logo_team2', '$match_score')";
        $upload = mysqli_query($conn,$insert);
        if($upload){
           move_uploaded_file($match_logo_tmp_name, $match_logo_folder);
           move_uploaded_file($match_logo_team1_tmp_name, $match_logo_team1_folder);
           move_uploaded_file($match_logo_team2_tmp_name, $match_logo_team2_folder);
           $message[] = 'Meciul a fost adăugat cu succes!';
        }else{
           $message[] = 'Nu s-a putut adăuga meciul.';
        }
     }

};

if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM matches WHERE id = $id");
    header('location:crud-match.php');
};


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Poli Timișoara</title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
        <link rel="stylesheet" href="styleCrud.css">
        <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
    </head>
    <body>
    <?php
         if(isset($message))
        {
            foreach($message as $message)
            {
                    echo '<span class="message">'.$message.'</span>';
            }
        }

?>
    <div class="container">

        <div class="admin-product-form-container">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <h3>Adaugă un nou meci</h3>
                <label style="font-family: 'Ubuntu', sans-serif;
                font-size: 15px;
                padding-top: 5px;">Stadion<input type="text" placeholder="Introdu stadionul" name="match_stadium" class="box">
                <label>Data<input type="date" placeholder="Introdu data și ora" name="match_time" class="box">
                <label>Echipa gazdă<input type="text" placeholder="Introdu echipa gazdă" name="match_team1" class="box">
                <label>Logo echipa gazdă<input type="file" accept="image/png, image/jpeg, image/jpg" name="match_logo_team1" class="box">
                <label>Echipa oaspete<input type="text" placeholder="Introdu echipa oaspete" name="match_team2" class="box">
                <label>Logo echipa oaspete<input type="file" accept="image/png, image/jpeg, image/jpg" name="match_logo_team2" class="box">
                <label>Scor<input type="text" placeholder="Introdu scorul (format: x - y)" name="match_score" class="box">
                <label>Logo competiție<input type="file" accept="image/png, image/jpeg, image/jpg" name="match_logo" class="box">
                <input type="submit" class="btn" name="add_match" value="ADAUGĂ MECI">
                
            </form>
            
        </div>

    <?php

        $select = mysqli_query($conn, "SELECT * FROM matches");
   
    ?>
   <div class="product-display">
      <table class="product-display-table">
         <thead>
         <tr>
            <th>ID</th>
            <th>Logo competiție</th>
            <th>Stadion</th>
            <th>Data</th>
            <th>Echipa gazdă</th>
            <th>Logo echipa gazdă</th>
            <th>Echipa oaspete</th>
            <th>Logo echipa oaspete</th>
            <th>Scor</th>
            <th>Opțiuni</th>
         </tr>
         </thead>
         <?php while($row = mysqli_fetch_assoc($select)){ ?>
         <tr>
            <td><?php echo $row['id']; ?></td>
            <td><img src="match_uploaded/<?php echo $row['logo']; ?>" height="100" alt=""></td>
            <td><?php echo $row['stadium']; ?></td>
            <td><?php echo $row['time']; ?></td>
            <td><?php echo $row['team1']; ?></td>
            <td><img src="match_uploaded/<?php echo $row['team1logo']; ?>" height="100" alt=""></td>
            <td><?php echo $row['team2']; ?></td>
            <td><img src="match_uploaded/<?php echo $row['team2logo']; ?>" height="100" alt=""></td>
            <td><?php echo $row['score']; ?></td>
            <td>
               <a href="crud-match-update.php?edit=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-edit"></i> edit </a>
               <a href="crud-match.php?delete=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-trash-o"></i> delete </a>
            </td>
         </tr>
         <?php } ?>
      </table>
   </div>

   <a href="crud-edit.html" class="btn">ÎNAPOI</a>

    </div>
    </body>
</html>