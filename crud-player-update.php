<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_player'])){

   $player_name = $_POST['player_name'];
   $player_nr = $_POST['player_nr'];
   $player_position = $_POST['player_position'];
   $player_image = $_FILES['player_image']['name'];
   $player_image_tmp_name = $_FILES['player_image']['tmp_name'];
   $player_image_folder = 'player_uploaded/'.$player_image;

   if(empty($player_name) || empty($player_nr) || empty($player_position)){
      $message[] = 'Completați toate câmpurile';    
   }else{

    if(!empty($player_image))
    {
       $update_data = "UPDATE players SET name='$player_name', number='$player_nr', position='$player_position', image='$player_image'  WHERE id = '$id'";
       $upload = mysqli_query($conn, $update_data);
       
    }
    else
      {
         $update_data = "UPDATE players SET name='$player_name', number='$player_nr', position='$player_position'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
      }

      if($upload){
         move_uploaded_file($player_image_tmp_name, $player_image_folder);
         header('location:crud-player.php');
      }else{
         $$message[] = 'Completați toate câmpurile'; 
      }

   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
</head>
<body>

<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">

   <datalist id="positions">
      <option value="PORTAR">
      <option value="FUNDAȘ">
      <option value="MIJLOCAȘ">
      <option value="ATACANT">
   </datalist>

<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM players WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">Editați informațiile jucătorului</h3>
      <label style="font-family: 'Ubuntu', sans-serif;
      font-size: 15px;
      padding-top: 5px;">Nume jucător<input type="text" class="box" name="player_name" value="<?php echo $row['name']; ?>" placeholder="Introduceți numele">
      <label>Număr echipament<input type="number" min="0" max="99" class="box" name="player_nr" value="<?php echo $row['number']; ?>" placeholder="Introdduceți numărul">
      <label>Poziție jucător<input list="positions" class="box" name="player_position" value="<?php echo $row['position']; ?>" placeholder="Introduceți poziția">
      <img src="player_uploaded/<?php echo $row['image']; ?>" height="100" alt=""><input type="file" class="box" name="player_image"  accept="image/png, image/jpeg, image/jpg">
      <input type="submit" value="UPDATEAZĂ JUCĂTORUL" name="update_player" class="btn">
      <a href="crud-player.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>