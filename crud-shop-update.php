<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_product'])){

    $product_name = $_POST['product_name'];
    $product_price = $_POST['product_price'];
    $product_image = $_FILES['product_image']['name'];
    $product_image_tmp_name = $_FILES['product_image']['tmp_name'];
    $product_image_folder = 'product_uploaded/'.$product_image;

   if(empty($product_name) || empty($product_price)){
      $message[] = 'Completați toate câmpurile';    
   }else{

    if(!empty($product_image))
    {
       $update_data = "UPDATE products SET name='$product_name', price='$product_price', image='$product_image'  WHERE id = '$id'";
       $upload = mysqli_query($conn, $update_data);
       
    }
    else
      {
         $update_data = "UPDATE products SET name='$product_name', price='$product_price'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
      }

      if($upload){
         move_uploaded_file($product_image_tmp_name, $product_image_folder);
         header('location:crud-shop.php');
      }else{
         $$message[] = 'Completați toate câmpurile'; 
      }

   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
</head>
<body>

<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">


<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM products WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">Editați informațiile produsului</h3>
      <label style="font-family: 'Ubuntu', sans-serif;
      font-size: 15px;
      padding-top: 5px;">Nume produs<input type="text" class="box" name="product_name" value="<?php echo $row['name']; ?>" placeholder="Introduceți numele">
      <label>Preț produs<input type="number" min="0" class="box" name="product_price" value="<?php echo $row['price']; ?>" placeholder="Introdduceți prețul">
      <img src="product_uploaded/<?php echo $row['image']; ?>" height="100" alt=""><input type="file" class="box" name="product_image"  accept="image/png, image/jpeg, image/jpg">
      <input type="submit" value="UPDATEAZĂ PRODUSUL" name="update_product" class="btn">
      <a href="crud-shop.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>