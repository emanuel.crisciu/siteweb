<?php

@include 'config.php';

if(isset($_POST['update_update_btn'])){
   $update_value = $_POST['update_quantity'];
   $update_id = $_POST['update_quantity_id'];
   $update_quantity_query = mysqli_query($conn, "UPDATE `cart` SET quantity = '$update_value' WHERE id = '$update_id'");
   if($update_quantity_query){
      header('location:cart.php');
   };
};

if(isset($_GET['remove'])){
   $remove_id = $_GET['remove'];
   mysqli_query($conn, "DELETE FROM `cart` WHERE id = '$remove_id'");
   header('location:cart.php');
};

if(isset($_GET['delete_all'])){
   mysqli_query($conn, "DELETE FROM `cart`");
   header('location:cart.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCart.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

</head>
<body>
<div class="navbar">
            <div class="navbar-left">
                <div class="navbar-left-text">POLI</div>
                <img class="navbar-logo" src="photos/poliLogo.png">
                <div class="navbar-left-text">TIMIȘOARA</div>
            </div>
            <div class="navbar-middle">
                <a href="index.php">
                    <div class="navbar-middle-button">ACASĂ</div>
                </a>
                <a href="echipa.php">
                    <div class="navbar-middle-button">ECHIPA</div>
                </a>
                <a href="meciuri.php">
                    <div class="navbar-middle-button">MECIURI</div>
                </a>
                <a href="shop.php">
                    <div class="navbar-middle-button">SHOP</div>
                </a>
            </div>
            <div class="navbar-right">
                <a href="logout.php">
                    <div class="navbar-login-button">LOGOUT</div>
                </a>
                <div>
                    <?php
                        $select_rows = mysqli_query($conn, "SELECT * FROM `cart`") or die('query failed');
                        $row_count = mysqli_num_rows($select_rows);
                    ?>
                    <a href="cart.php">
                        <button class="navbar-cart-button"><i class="fas fa-shopping-cart"></i> <?php echo $row_count; ?></button>
                    </a>
                </div>
            </div>
</div>
<div class="container">

<section class="shopping-cart">

   <h1 class="heading">Coșul de produse</h1>

   <table>

      <thead>
         <th>Imagine</th>
         <th>Produs</th>
         <th>Preț</th>
         <th>Cantitate</th>
         <th>Subtotal</th>
         <th>Opțiuni</th>
      </thead>

      <tbody>

         <?php 
         
         $select_cart = mysqli_query($conn, "SELECT * FROM `cart`");
         $grand_total = 0;
         if(mysqli_num_rows($select_cart) > 0){
            while($fetch_cart = mysqli_fetch_assoc($select_cart)){
         ?>

         <tr>
            <td><img src="product_uploaded/<?php echo $fetch_cart['image']; ?>" height="100" alt=""></td>
            <td><?php echo $fetch_cart['name']; ?></td>
            <td><?php echo number_format($fetch_cart['price']); ?> lei / buc</td>
            <td>
               <form action="" method="post">
                  <input type="hidden" name="update_quantity_id"  value="<?php echo $fetch_cart['id']; ?>" >
                  <input type="number" name="update_quantity" min="1"  value="<?php echo $fetch_cart['quantity']; ?>" >
                  <input type="submit"  value="UPDATE"  name="update_update_btn">
               </form>   
            </td>
            <td><?php echo $sub_total = number_format($fetch_cart['price'] * $fetch_cart['quantity']); ?> lei</td>
            <td><a href="cart.php?remove=<?php echo $fetch_cart['id']; ?>" onclick="return confirm('Ștergeți articolul din coș?')" class="delete-btn">ȘTERGEȚI</a></td>
         </tr>
         <?php
           $grand_total += $sub_total;  
            };
         };
         ?>
         <tr class="table-bottom">
            <td><a href="shop.php" class="option-btn" >CONTINUAȚI CUMPĂRĂTURILE</a></td>
            <td colspan="3" style="font-weight: 500;">TOTAL</td>
            <td style="font-weight: 500;"><?php echo $grand_total; ?> lei</td>
            <td style="font-weight: 500;"><a href="cart.php?delete_all" onclick="return confirm('Sunteți sigur că doriți să ștergeți toate articolele?');" class="delete-btn">ȘTERGEȚI TOT</a></a></td>
         </tr>
      </tbody>

   </table>

   <div class="checkout-btn">
      <a href="order.php" style="width: 100%;" class="btn <?= ($grand_total > 0)?'':'disabled'; ?>">CONTINUAȚI CĂTRE COMANDĂ</a>
   </div>

</section>

</div>

</body>
</html>