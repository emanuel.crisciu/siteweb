<?php

@include 'config.php';

if(isset($_POST['add_to_cart'])){

   $product_name = $_POST['product_name'];
   $product_price = $_POST['product_price'];
   $product_image = $_POST['product_image'];
   $product_quantity = 1;

   $select_cart = mysqli_query($conn, "SELECT * FROM `cart` WHERE name = '$product_name'");

   if(mysqli_num_rows($select_cart) > 0){
      $message[] = 'Produsul a fost deja adăugat în coș!';
   }else{
      $insert_product = mysqli_query($conn, "INSERT INTO `cart`(name, price, image, quantity) VALUES('$product_name', '$product_price', '$product_image', '$product_quantity')");
      $message[] = 'Produsul a fost adăugat cu succes!';
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleShop.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
   
</head>
<body>

    <div class="navbar">
            <div class="navbar-left">
                <div class="navbar-left-text">POLI</div>
                <img class="navbar-logo" src="photos/poliLogo.png">
                <div class="navbar-left-text">TIMIȘOARA</div>
            </div>
            <div class="navbar-middle">
                <a href="index.php">
                    <div class="navbar-middle-button">ACASĂ</div>
                </a>
                <a href="echipa.php">
                    <div class="navbar-middle-button">ECHIPA</div>
                </a>
                <a href="meciuri.php">
                    <div class="navbar-middle-button">MECIURI</div>
                </a>
                <a href="shop.php">
                    <div class="navbar-middle-button">SHOP</div>
                </a>
            </div>
            <div class="navbar-right">
                <a href="logout.php">
                    <div class="navbar-login-button">LOGOUT</div>
                </a>
                <div>
                    <?php
                        $select_rows = mysqli_query($conn, "SELECT * FROM `cart`") or die('query failed');
                        $row_count = mysqli_num_rows($select_rows);
                    ?>
                    <a href="cart.php">
                        <button class="navbar-cart-button"><i class="fas fa-shopping-cart"></i> <?php echo $row_count; ?></button>
                    </a>
                </div>
            </div>
    </div>

    <?php

    if(isset($message)){
    foreach($message as $message){
        echo '<div style="
        top:0; left:0;
        z-index: 10000;
        border-radius: .5rem;
        background-color: rgba(229, 229, 229, 0.721);
        font-family: sans-serif;
        font-size: 1.1rem;
        font-weight: bold;
        color: rgb(92, 32, 92);
        padding:1.5rem 2rem;
        margin: 85px auto 0px auto;
        max-width: 1200px;
        display: flex;
        align-items: center;
        justify-content: space-between;"><span>'.$message.'</span> <i class="fas fa-times" onclick="this.parentElement.style.display = `none`;"></i> </div>';
    };
    };

    ?>

    <div class="shop-zone-title">SHOP</div>
    <div class="shop-zone">

            <?php
            
                $select_products = mysqli_query($conn, "SELECT * FROM `products`");
                if(mysqli_num_rows($select_products) > 0){
                    while($fetch_product = mysqli_fetch_assoc($select_products)){

            ?>

            <form action="" method="post">
                <div class="product">
                    <div class="product-photo">
                        <img src="product_uploaded/<?php echo $fetch_product['image']; ?>" class="product-photo-image">
                    </div>
                    <div class="product-name"><?php echo $fetch_product['name']; ?></div>
                    <div class="product-price-zone">
                        <div class="product-price"><?php echo $fetch_product['price']; ?> lei</div>
                        
                        <input type="submit" class="product-add" value="Adaugă în coș" name="add_to_cart">
                    </div>
                    
                    <input type="hidden" name="product_name" value="<?php echo $fetch_product['name']; ?>">
                    <input type="hidden" name="product_price" value="<?php echo $fetch_product['price']; ?>">
                    <input type="hidden" name="product_image" value="<?php echo $fetch_product['image']; ?>">
                    
                </div>
            </form>
            
            <?php
            };
            };
            ?>

    </div>

    <div class="news-header">Știri</div>   
    <div class="news-zone">
            <div class="news">
                <a href="nouaEchipaNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/adunare.jpeg"></div></a>
                <div class="news-title">Un nou drum, o nouă echipă</div>
            </div>
            <div class="news">
                <a href="stadionNews.html"><div class="news-photo"><img class="news-photo-image" src="photos/stadion.webp"></div></a>
                <div class="news-title">Noul stadion din Timișoara</div>
            </div>
            <div class="news">
                <a href="celtic43News.html"><div class="news-photo"><img class="news-photo-image" src="photos/Poli-Celtic-1980.jpg"></div></a>
                <div class="news-title">43 de ani de la eliminarea lui Celtic</div>
            </div>
    </div>
    <div class="map-text">Unde ne găsiți?</div>
    <div class="map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2784.532772010323!2d21.241458875876432!3d45.740464114964084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47455d92ebd3d733%3A0xf3fdfa7034ef15b8!2sStadionul%20Dan%20P%C4%83ltini%C8%99anu!5e0!3m2!1sro!2sro!4v1696682882466!5m2!1sro!2sro"
                width="1300" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
    <div class="partners-zone">
            <div class="partners-title">PARTENERI</div>
            <div class="partners">
                <div class="partners-photo"><a href="https://www.errea.com/world/" target="_blank"><img class="partners-photo-image" src="photos/ERREA-logo.png"></a></div>
                <div class="partners-photo"><a href="https://www.upt.ro/" target="_blank"><img class="partners-photo-image" src="photos/logo_UPT.jpg"></div></a>
                <div class="partners-photo"><a href="https://www.druckeria.ro/" target="_blank"><img class="partners-photo-image" src="photos/Druckeria-logo.png"></div></a>
                <div class="partners-photo"><a href="https://www.mewi.ro/" target="_blank"><img class="partners-photo-image" src="photos/MEWI.jpg"></div></a>
                <div class="partners-photo"><a href="https://www.casa-bunicii.ro/" target="_blank"><img class="partners-photo-image" src="photos/restaurant.png"></div></a>
                <div class="partners-photo"><a href="https://apuseana.ro/" target="_blank"><img class="partners-photo-image" src="photos/apuseana.png"></div></a>
                <div class="partners-photo"><a href="https://www.unibet.ro/" target="_blank"><img class="partners-photo-image" src="photos/unibet2429.jpg"></div></a>
            </div>
    </div>
    </div>

</body>
</html>