<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_match'])){

   $match_stadium = $_POST['match_stadium'];
   $match_time = $_POST['match_time'];
   $match_team1 = $_POST['match_team1'];
   $match_team2 = $_POST['match_team2'];
   $match_score = $_POST['match_score'];

   $match_logo = $_FILES['match_logo']['name'];
   $match_logo_tmp_name = $_FILES['match_logo']['tmp_name'];
   $match_logo_folder = 'match_uploaded/'.$match_logo;

   $match_logo_team1 = $_FILES['match_logo_team1']['name'];
   $match_logo_team1_tmp_name = $_FILES['match_logo_team1']['tmp_name'];
   $match_logo_team1_folder = 'match_uploaded/'.$match_logo_team1;

   $match_logo_team2 = $_FILES['match_logo_team2']['name'];
   $match_logo_team2_tmp_name = $_FILES['match_logo_team2']['tmp_name'];
   $match_logo_team2_folder = 'match_uploaded/'.$match_logo_team2;

   if(empty($match_stadium) || empty($match_time) || empty($match_team1) || empty($match_team2) || empty($match_score))
   {
    $message[] = 'Completați toate câmpurile';
   }else{

    if(!empty($match_logo) && !empty($match_logo_team1) && !empty($match_logo_team2))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', logo='$match_logo', team1logo='$match_logo_team1', team2logo='$match_logo_team2'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo_team1) && !empty($match_logo_team2))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', team1logo='$match_logo_team1', team2logo='$match_logo_team2'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo_team1) && !empty($match_logo))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', team1logo='$match_logo_team1', logo='$match_logo'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo_team2) && !empty($match_logo))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', team2logo='$match_logo_team2', logo='$match_logo'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', logo='$match_logo'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo_team1))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', team1logo='$match_logo_team1'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else if(!empty($match_logo_team2))
    {
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score', team2logo='$match_logo_team2'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }
    else{
        $update_data = "UPDATE matches SET stadium='$match_stadium', time='$match_time', team1='$match_team1', team2='$match_team2', score='$match_score'  WHERE id = '$id'";
        $upload = mysqli_query($conn, $update_data);
    }

    if($upload){
        move_uploaded_file($match_logo_tmp_name, $match_logo_folder);
        move_uploaded_file($match_logo_team1_tmp_name, $match_logo_team1_folder);
        move_uploaded_file($match_logo_team2_tmp_name, $match_logo_team2_folder);
        header('location:crud-match.php');
    }else{
        $$message[] = 'Completați toate câmpurile'; 
    }

   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Poli Timișoara</title>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="photos/transparent-poliLogo.png">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
</head>
<body>

<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">


<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM matches WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">Editați informațiile meciului</h3>
      <label style="font-family: 'Ubuntu', sans-serif;
      font-size: 15px;
      padding-top: 5px;">Stadion<input type="text" class="box" name="match_stadium" value="<?php echo $row['stadium']; ?>" placeholder="Introduceți stadionul">
      <label>Data<input type="date" class="box" name="match_time" value="<?php echo $row['time']; ?>" placeholder="Introduceți data și ora">
      <label>Echipa gazdă<input type="text" class="box" name="match_team1" value="<?php echo $row['team1']; ?>" placeholder="Introduceți echipa gazdă">
      <img src="match_uploaded/<?php echo $row['team1logo']; ?>" height="100" alt=""><input type="file" class="box" name="match_logo_team1"  accept="image/png, image/jpeg, image/jpg">
      <label>Echipa oaspete<input type="text" class="box" name="match_team2" value="<?php echo $row['team2']; ?>" placeholder="Introduceți echipa oaspete">
      <img src="match_uploaded/<?php echo $row['team2logo']; ?>" height="100" alt=""><input type="file" class="box" name="match_logo_team2"  accept="image/png, image/jpeg, image/jpg">
      <label>Scor<input type="text" class="box" name="match_score" value="<?php echo $row['score']; ?>" placeholder="Introduceți scorul (format: x - y)">
      <img src="match_uploaded/<?php echo $row['logo']; ?>" height="100" alt=""><input type="file" class="box" name="match_logo"  accept="image/png, image/jpeg, image/jpg">
      <input type="submit" value="UPDATEAZĂ MECIUL" name="update_match" class="btn">
      <a href="crud-match.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>